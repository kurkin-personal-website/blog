+++
title = "Static & Dynamic Type Systems"
description = "Short explanation of the difference between static and dynamic type systems."
+++

Let's talk a bit about static and dynamic languages. I know the topic has been discussed ad nauseam
and the majority of those who write software or learning how to do it have already chosen their stance on the matter.
What leaves me perplexed though is that the choice of using a language with certain type system is perceived as
either-or thing. Too many people think that if you know a certain language and you need to write a software solution,
you'd better stick to the thing you already know -- the majority of the programming languages out there are general
purpose after all.

However, the choice of sticking to the thing you know best might not be always optimal. Programming languages are tools,
even if they're tools with a huge applicability. We don't argue that a laptop and a tablet have their pros and cons, then
why can't we think the same about programming languages and their type systems? Let's try to explore that.

## Dynamic Languages

Dynamic languages prioritize flexibility and freedom.
In these languages, programmers don't need to explicitly declare variable types and think about the types in advance.
This means you can use variables of any type in functions and expressions without strict type checking and rely on
what's called [duck typing](https://en.wikipedia.org/wiki/Duck_typing).
Programs written in dynamic languages are usually [interpreted](https://en.wikipedia.org/wiki/Interpreter_(computing)).

For example, consider the `sum_of_two` function in **Python**.
It can effortlessly handle various types of inputs, whether they're integers, floats, strings or even lists:

```python
def sum_of_two(number1, number2):
    return number1 + number2
```

```python
>>> sum_of_two(3, 4)
7
```

```python
>>> sum_of_two(3.3, 4.4)
7.7
>>> sum_of_two(2, 3.3)
5.3
```

```python
>>> sum_of_two("3", "4")
'34'
```

```python
>>> sum_of_two([1, 2, 3], [5, 6, 7])
[1, 2, 3, 5, 6, 7]
```

While this flexibility allows for creative solutions,
it also places the responsibility on the programmer to ensure that types are used correctly.

Other famous dynamic languages: *JavaScript*, *PHP*, *Ruby*, *Lua*.

## Static Languages

On the other hand, static languages emphasize safety.
In these languages, you must explicitly declare variable types, and the compiler enforces strict type checking.
This means you can't pass a variable of the wrong type to a function or expression without encountering an error.
Programs written in static languages are usually [compiled](https://en.wikipedia.org/wiki/Compiler)
(or [transpiled](https://en.wikipedia.org/wiki/Source-to-source_compiler)).

Take the `sumTwo` function in **Go** as an example:

```go
func sumTwo(number1 int, number2 int) int {
	return number1 + number2
}
```

```go
// sum_two.go
func main() {
	fmt.Println(sumTwo(3, 4))
}
```

```bash
$ go run sum_two.go
7
```

```go
// sum_two.go
func main() {
	fmt.Println(sumTwo(3.4, 4.3))
}
```

```bash
$ go run sum_two.go
./sum_two.go:10:28: constant 3.4 truncated to integer
./sum_two.go:10:33: constant 4.3 truncated to integer
```

```go
// sum_two.go
func main() {
	fmt.Println(sumTwo("3", "4"))
}
```

```bash
$ go run sum_two.go
./sum_two.go:10:28: cannot use "3" (type untyped string) as type int in argument to sumTwo
./sum_two.go:10:33: cannot use "4" (type untyped string) as type int in argument to sumTwo
```

It only accepts integers, and attempting to pass it a float or a string will result in a compile-time error.
While this approach may seem more restrictive, it offers greater assurance that your code is type-safe and free from certain classes of errors.

Other famous static languages: *Java*, *TypeScript*, *Swift*, *Rust*.

## Choosing the Right Tool for the Job

So where do the examples above bring us? Both dynamic and static languages have their strengths and weaknesses, and the choice between them depends on the specific needs of your project. Dynamic languages offer flexibility and rapid prototyping, making them well-suited for tasks that require quick iteration and experimentation. Static languages, on the other hand, provide greater safety and performance optimization, making them ideal for large-scale projects and mission-critical applications.